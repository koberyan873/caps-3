import { useState, useContext } from "react";
import { Link } from "react-router-dom";
import { Navbar, Container, Nav } from "react-bootstrap";
import UserContext from "../UserContext";
import "../App.css";

export default function AppNavbar() {
  const { user } = useContext(UserContext);

  // state hook to store the user information stored in the login page
  // const [user, setUser] = useState(localStorage.getItem("email"));
  // console.log(user);

  return (
    <Navbar expand="lg" className="sticky-top">
      <Container className="ms-5" id="navbar" fluid>
        <Navbar.Brand as={Link} to="/" id="navBrand">
          The General's Coffee
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="ms-auto">
            <Nav.Link as={Link} to="/">
              Barracks
            </Nav.Link>
            <Nav.Link as={Link} to="/products">
              Assets
            </Nav.Link>

            {user.id !== null ? (
              <>
                <Nav.Link as={Link} to="/getAllProducts">
                  Command Center
                </Nav.Link>
                <Nav.Link as={Link} to="/profile">
                  Bio
                </Nav.Link>
                <Nav.Link as={Link} to="/logout">
                  Fall Out
                </Nav.Link>
              </>
            ) : (
              <>
                <Nav.Link as={Link} to="/login">
                  Login
                </Nav.Link>
                <Nav.Link as={Link} to="/register">
                  Register
                </Nav.Link>
              </>
            )}
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}
