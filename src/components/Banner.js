import { Row, Col, Button } from "react-bootstrap";
import { Link } from "react-router-dom";
import tgc from "../assets/genmug-logo.png";
export default function Banner() {
  return (
    <div id="banner">
      <img src={tgc} className="photo" alt="coffee" />
    </div>
  );
}
