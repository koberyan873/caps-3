import { useState, useContext } from "react";
import { Card, Button, Row, Col } from "react-bootstrap";
import { Link } from "react-router-dom";
import UserContext from "../UserContext";

import sound from "../assets/soldiersvoices_sfxb.mp3";

export default function CourseCard({ productProp }) {
  const { user } = useContext(UserContext);
  const { name, description, price, _id, image } = productProp;

  function play() {
    new Audio(sound).play();
  }

  return (
    <Row className="mt-3 mb-3">
      <Col className="d-flex">
        <Card className=" mx-auto" style={{ width: "20%" }}>
          <Card.Img variant="top" src={image} alt={name} />
          <Card.Body>
            <Card.Title>{name}</Card.Title>
            <Card.Text>{description}</Card.Text>
            <Card.Subtitle>Price</Card.Subtitle>
            <Card.Text>{price}</Card.Text>
            {user._id !== null ? (
              <>
                <Link
                  className="btn btn-success"
                  to={`/productView/${_id}`}
                  onClick={play}
                >
                  Buy Now
                </Link>
              </>
            ) : (
              <>
                <Link className="btn btn-primary" to={`/login`}>
                  Details
                </Link>
              </>
            )}
          </Card.Body>
        </Card>
      </Col>
    </Row>
  );
}
