import { Fragment, useState, useEffect, useContext } from "react";
import { Table, Button, Modal, Form } from "react-bootstrap";

import AdminCard from "../components/AdminCard";
import UserContext from "../UserContext";
import Swal from "sweetalert2";

export default function AdminPortal() {
  const { user } = useContext(UserContext);

  const [name, setName] = useState("");
  const [desciption, setDescription] = useState("");
  const [price, setPrice] = useState("");
  const [image, setImage] = useState("");
  const [product, setProduct] = useState("");
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  function refresh() {
    fetch(`${process.env.REACT_APP_API_URL}/products/getAllProducts`, {
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    })
      .then((res) => res.json())
      .then((data) => {
        // console.log(data);
        setProduct(
          data.map((product) => {
            return (
              <AdminCard
                key={product._id}
                productProp={product}
                refresh={refresh}
              />
            );
          })
        );
      });
  }
  //useEffect function
  useEffect(() => {
    refresh();
  }, []);

  //end useEffect function

  //addProduct Function
  const addProduct = (token) => {
    //TODO:TESTING
    // console.log("im here");

    fetch(`${process.env.REACT_APP_API_URL}/products/addProducts`, {
      method: "POST",
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
        "Content-type": "application/json"
      },
      body: JSON.stringify({
        name: name,
        description: desciption,
        price: price,
        image: image,
        isActive: true
      })
    })
      .then((res) => res.json())
      .then((data) => {
        //TODO:TESTING
        Swal.fire({
          title: "Product Added",
          icon: "success"
        });
        refresh();

        // setProduct({
        //     name: data.name
        // });

        // Clear input fields after submission
        setName("");
      });
  };
  //end addProduct Function

  return (
    <>
      {user.isAdmin === true ? (
        <Button
          variant="info"
          className="p-2 mb-3 ms-auto"
          onClick={handleShow}
        >
          Add New
        </Button>
      ) : (
        <h1 className="noAccess">Restricted Area: You don't have clearance!</h1>
      )}

      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Add/Edit Product</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
              <Form.Label>Product Name</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter product name"
                required
                value={name}
                onChange={(e) => setName(e.target.value)}
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
              <Form.Label>Price</Form.Label>
              <Form.Control
                type="number"
                placeholder="Enter Price"
                required
                value={price}
                onChange={(e) => setPrice(e.target.value)}
              />
            </Form.Group>
            <Form.Group
              className="mb-3"
              controlId="exampleForm.ControlTextarea1"
            >
              <Form.Label>Description</Form.Label>
              <Form.Control
                as="textarea"
                rows={3}
                type="number"
                placeholder="Description"
                required
                value={desciption}
                onChange={(e) => setDescription(e.target.value)}
              />
            </Form.Group>

            <Form.Group controlId="productImage">
              <Form.Label>Product Image URL</Form.Label>
              <Form.Control
                type="text"
                name="productImage"
                value={image}
                onChange={(e) => setImage(e.target.value)}
                required
              />
            </Form.Group>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button variant="primary" onClick={addProduct}>
            Add Product
          </Button>
        </Modal.Footer>
      </Modal>

      {product}
    </>
  );
}
