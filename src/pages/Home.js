import Banner from "../components/Banner";
import Highlights from "../components/Highlights";
import { Link } from "react-router-dom";
import { Button } from "react-bootstrap";
import "../App.css";
import river from "../assets/river.mp3";
import { useState, useEffect } from "react";

export default function Home() {
  const [value, setValue] = useState(0);

  useEffect(() => {
    if (value % 2 === 0) play();
  }, [value]);

  function play() {
    new Audio(river).play();
  }

  return (
    <>
      <div id="wholebg" onClick={() => setValue(value + 1)}>
        <Banner />
      </div>
      <div id="btn">
        <Button
          className="shopNow"
          id="btn"
          variant="success"
          as={Link}
          to="/products"
        >
          SHOP NOW!
        </Button>
      </div>
      <Highlights />
    </>
  );
}
