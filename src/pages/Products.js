import coursesData from "../data/coursesData";
import ProductCard from "../components/ProductCard";
import { useState, useEffect } from "react";

export default function Courses() {
  const [products, setProducts] = useState([]);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/activeProducts`)
      .then((res) => res.json())
      .then((data) => {
        // console.log(data);

        setProducts(
          data.map((product) => {
            return <ProductCard key={product._id} productProp={product} />;
          })
        );
      });
  }, []);

  return (
    <>
      <h1 className="available">Available Assets</h1>
      {products}
    </>
  );
}
